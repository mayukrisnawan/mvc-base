{% if navigations|length > 0 %}
<ol class="breadcrumb">
  {% for label,url in navigations %}
    <li {{ loop.index == navigations|length ? "class='active'" : "" }}>
      {% if loop.index == navigations|length %}
        {{ label }}
      {% else %}
        <a href="{{ url }}">{{ label }}</a>
      {% endif %}
    </li>
  {% endfor %}
</ol>
{% endif %}