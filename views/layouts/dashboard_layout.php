<!DOCTYPE html>
<html>
<head>
  <title>Manajemen SAP dan Silabus</title>
  <?php
    css(array(
      'bootstrap',
      'jquery.mmenu'
    ));
    js(array(
      'jquery-1.10.2.min',
      'bootstrap.min',
      'jquery.validate.min',
      'jquery.mmenu.min',
      'app'
    )); 
  ?>
</head>
<body>
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Tampilkan/Sembunyikan Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php eu('home#index'); ?>">Manajemen SAP dan Silabus</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <!--<li class="active"><a href="#">Home</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">
          {% block right_nav %}{% endblock %}
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
  <div class="container">
    <div class="row">
      {% include "shared/_app_menu.php" %}
      <div class="panel panel-default col-md-29 col-md-offset-1">
        <div class="panel-body">
          {% include "shared/_breadcrumb.php" %}
          {% block body %}{% endblock %}
        </div>
      </div>
    </div>
  </div>
</body>
</html>