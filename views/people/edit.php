{% extends "layouts/dashboard_layout.php" %}

{% block body %}
{{ a('&lt;&lt; Daftar Pengunjung', 'people#index') }}
<h1>Edit Data Pengunjung >> {{ person.name }}</h1>
<div class="row">
  <div class="col-md-10">
    {% include 'people/form.php' %}
  </div>
</div>
{% endblock %}