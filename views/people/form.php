<form role="form" name='person_form' action='{{ u(action, {id:person.id}) }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="person[id]" value="{{ person.id }}">
  <div class="form-group">
    <label class="control-label">Nama</label>
    <input name="person[name]" type="text" class="form-control" value="{{ person.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("person"); ?>
</script>