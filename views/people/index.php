{% extends "layouts/dashboard_layout.php" %}

{% block body %}
<h1>Halaman Dashboard</h1>
<span class='label label-danger'>
  <span class="glyphicon glyphicon-user"></span>
  Daftar pengunjung
</span>
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah pengunjung baru", "people#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Pilihan</th>
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="3">
          <center>
            <i>Data pengunjung belum tersedia</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for person in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ a(person.name, 'people#show', {id:person.id}) }}
        </td>
        <td>{{ a("Hapus", "people#destroy", {id:person.id}, {method:"delete", remote:"true", 
          confirmTitle:"Data Pengunjung",
          confirmMessage:"Apakah anda yakin ingin menghapus pengunjung berikut?",
          class:"btn_hapus"}) }}</td> 
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}

{% block right_nav %}
{% include "shared/_login_nav.php" %}
{% endblock %}