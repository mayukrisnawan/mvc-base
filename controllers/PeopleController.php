<?php

class PeopleController extends Controller {
  # GET /resource
  public function index() {
    $this->data->navigations = array(
      "Dashboard"=>u("admin#index"),
      "Daftar Pengunjung"=>u("people#index")
    );
    $this->data->pagination = Person::paginate(array("order"=>"name"));
    $this->render("people/index");
  }

  # GET /resource/:id
  public function show($id) {
    try {
      $this->data->person = Person::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "people#update"; 
    $this->data->method = "patch"; 
    $this->render("people/edit");
  }

  # GET /resource/:id/edit
  public function edit($id) {
    try {
      $this->data->person = Person::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "people#update"; 
    $this->data->method = "patch"; 
    $this->render("people/edit");
  }

  # GET /resource/add
  public function add() {
    $this->data->action = "people#create"; 
    $this->data->method = "post"; 
    $this->render("people/add");
  }

  # POST /resource/:id
  public function create() {
    $person = new Person();
    foreach (params("person") as $key => $value) {
      $person->$key = $value;
    }
    if ($person->save()) {
      $this->render("people/create_success");
    } else {
      $this->data->errors = $person->errors->fetch();
      $this->render("people/create_errors");
    }
  }

  # PATCH /resource/:id
  public function update($id) {
    try {
      $person = Person::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    foreach (params("person") as $key => $value) {
      $person->$key = $value;
    }
    if ($person->save()) {
      $this->render("people/update_success");
    } else {
      $this->data->errors = $person->errors->fetch();
      $this->render("people/update_errors");
    }
  }

  # DELETE /resource/:id
  public function destroy($id) {
    try {
      $person = Person::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($person->delete()) {
      $this->render("people/destroy_success");
    } else {
      $this->render("people/destroy_errors");
    }
  }
}