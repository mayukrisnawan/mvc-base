<?php

Router::set("/", "home#index");
Router::accept("home");

Router::set("/admin", "admin#index");
Router::accept("admin");

Router::rest("people");