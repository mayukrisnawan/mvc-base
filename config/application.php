<?php

return array(
  'current_mode' => 'development',
  'connections' => array(
    'development' => 'mysql://root@localhost/sap1',
    'production' => 'mysql://username:password@localhost/production'
  )
);

