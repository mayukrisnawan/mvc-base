<?php

require_once "system/vendors/php-activerecord/ActiveRecord.php";
require_once 'system/vendors/php-activerecord/lib/Validations.php';
require_once "system/Pagination.php";

ActiveRecord\Config::initialize(function($cfg)
{
     $cfg->set_model_directory('models');
     $cfg->set_connections(App::config("connections"));
     $cfg->set_default_connection(App::config("current_mode"));
});

class Model extends ActiveRecord\Model {
  protected static $itemPerPage = 10;
  protected static $paginator = null;
  public static function paginate(array $options=array()) {
    static::$paginator = new Pagination(static::$itemPerPage, "_page");
    $countOptions = $options;
    $pk = static::$primary_key ? static::$primary_key : "id";
    $countOptions["select"] = "count($pk) AS cnt";
    static::$paginator->set_total(static::first($countOptions)->cnt);

    if (!isset($options["limit"])) $options["limit"] = static::$paginator->limit();
    if (!isset($options["offset"])) $options["offset"] = static::$paginator->offset();

    $result = new MetaData();
    $result->records = static::find("all", $options);
    $result->links = static::$paginator->page_links();
    $result->limit = static::$paginator->limit();
    $result->offset = static::$paginator->offset();
    return $result;
  }

  public static function validationRules() {
    $className = get_called_class();
    $validator = new ActiveRecord\Validations(new $className);
    return $validator->rules();
  }
}