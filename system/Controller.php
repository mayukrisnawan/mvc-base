<?php

require_once "system/vendors/Twig/Autoloader.php";
require_once "system/MetaData.php";

class Controller {
  private $engine = null;
  private $viewLocation = "./views/";
  private $cacheLocation = "./system/tmp/";
  private $viewExtensions = array(".twig", ".js", ".html", ".php");
  protected $data = null;

  public function __construct() {
    Twig_Autoloader::register();
    $this->engine = new Twig_Environment(
      new Twig_Loader_Filesystem($this->viewLocation)//,array("cache"=>$this->cacheLocation)
    );
    $engineExtension = include("system/helpers/twig_extensions.php");
    $engineExtension($this->engine);
    $this->data = new MetaData();
  }

  public function __call($method, $args) {
    if ($method == "render") {
      // Penyusunan nama view
      $name = $args[0];
      $path = $this->viewLocation . $name . $this->viewExtensions[0];
      $currentExt = $this->viewExtensions[0];
      foreach ($this->viewExtensions as $ext) {
        $currentExt = $ext;
        $path = $this->viewLocation . $name . $ext;
        if (file_exists($path)) break;
      }
      if (!file_exists($path)) throw new Exception("View dengan nama $name tidak ditemukan");

      // Fetching data
      $data = $this->data->all();
      $dataAdders = isset($args[1]) ? $args[1] : array();
      foreach ($data as $key => $value) {
        $data[$key] = isset($dataAdders[$key]) ? $dataAdders[$key] : $value;
        $data[$key] = MetaData::plain($data[$key]);
      }

      // Proses render yang sebenarnya
      $tpl = $this->engine->loadTemplate($name . $currentExt);
      $contents = $tpl->render($data);
      file_put_contents("system/tmp/" . md5($name), $contents, LOCK_EX);
      include "system/tmp/" . md5($name);
    }
  }
}