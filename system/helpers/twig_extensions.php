<?php

return function($engine){
  // url function
  $u = new Twig_SimpleFunction('u', function($name, array $params=array()){
    return u($name, $params);
  });

  // anchor function
  $a = new Twig_SimpleFunction('a', function($label, $routeName, array $routeParams=array(), array $anchorAttributes=array()){
    try {
      $url = u($routeName, $routeParams);
    } catch (Exception $e) {
      $url = $routeName;
    }
    $attr = "";
    foreach ($anchorAttributes as $key => $value) {
      $attr .= " " . $key . "='" . $value . "'";
    }
    return "<a href='$url'".$attr.">$label</a>";
  }, array(
    'is_safe'=>array('html')
  ));

  // extending the engine
  $engine->addFunction($u);
  $engine->addFunction($a);
}

?>