<?php

/*
Validasi yang bisa diotomatiskan :
 - validates_presence_of
 - validates_length_of
 - validates_size_of
 - validates_format_of
 - validates_numericality_of
*/

function jQueryValidate($modelName, array $customRules=array(), array $customRules=array(), array $customMessages=array()) {
  $modelName = strtolower($modelName);
  $rules = array();
  $messages = array();
  $className = ucfirst($modelName);
  $rawRules = $className::validationRules();
  $regexCount = 0;

  // Fetching rules from model
  foreach ($rawRules as $attribute=>$validation) {
    if (!isset($rules[$modelName."[".$attribute."]"])) $rules[$modelName."[".$attribute."]"] = array();
    foreach ($validation as $rule) {
      $types = array();
      switch ($rule["validator"]) {
        case "validates_presence_of":
          $types["required"]=array("value"=>true, "message"=>$rule["message"]);
          break;
        case "validates_length_of":
          if (isset($rule["maximum"])) {
            $types["maxlength"]=array("value"=>$rule["maximum"], "message"=>$rule["too_long"]);
          }
          if (isset($rule["minimum"])) {
            $types["minlength"]=array("value"=>$rule["minimum"], "message"=>$rule["too_short"]);
          }
          break;
        case "validates_size_of":
          if (isset($rule["maximum"])) {
            $types["maxLength"]=array("value"=>$rule["maximum"], "message"=>$rule["message"]);
          }
          if (isset($rule["minimum"])) {
            $types["minLength"]=array("value"=>$rule["minimum"], "message"=>$rule["message"]);
          }
          break;
        case "validates_numericality_of":
          if (isset($rule["only_integer"])) {
            if ($rule["only_integer"] === true) {
              $types["digits"]=array("value"=>true, "message"=>$rule["message"]);
            }
          }
          if (isset($rule["greater_than"])) {
            $types["min"]=array("value"=>$rule["greater_than"]+1, "message"=>$rule["message"]);
          }
          if (isset($rule["greater_than_or_equal_to"])) {
            $types["min"]=array("value"=>$rule["greater_than_or_equal_to"], "message"=>$rule["message"]);
          }
          if (isset($rule["even"])) {
            if ($rule["even"] === true) {
              $types["even"]=array("value"=>true, "message"=>$rule["message"]);
            }
          }
          if (isset($rule["odd"])) {
            if ($rule["odd"] === true) {
              $types["odd"]=array("value"=>true, "message"=>$rule["message"]);
            }
          }
          if (isset($rule["less_than"])) {
            $types["max"]=array("value"=>$rule["less_than"]-1, "message"=>$rule["message"]);
          }
         if (isset($rule["less_than_or_equal_to"])) {
            $types["max"]=array("value"=>$rule["less_than_or_equal_to"], "message"=>$rule["message"]);
          }             
          break;
        case "validates_format_of":
          if (isset($rule["with"])) {
            $regexName = "regex_".$modelName."_".$attribute."_".$regexCount;
            $regex = $rule["with"];
            echo '$.validator.addMethod(
              "'.$regexName.'",
              function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
              },
              "Format salah"
            );'."\n";
            if (strlen($regex) > 2) {
              $regex = substr($regex, 1, strlen($regex)-2);
              $types[$regexName]=array("value"=>stripslashes($regex), "message"=>$rule["message"]);
              $regexCount++;
            }
          }
          break;
      }
      foreach ($types as $type=>$data) {
        $rules[$modelName."[".$attribute."]"][$type] = $data["value"];
        $messages[$modelName."[".$attribute."]"][$type] = ucfirst($data["message"]);
      }
    }
  }
  $rules = array_merge($rules, $customRules);
  $messages = array_merge($messages, $customMessages);
  $rules = count($rules) == 0 ? "{}" : json_encode($rules);
  $messages = count($messages) == 0 ? "{}" : json_encode($messages);
  echo "$(\"form[name='".$modelName."_form']\").validate({
    submitHandler:function(){
      ajaxSubmit(document.forms.".$modelName."_form, $(\"form[name='".$modelName."_form']\").find('input[type=\"submit\"]'));
      return false;
    },
    rules:$rules,
    messages:$messages,
    errorPlacement: function(error, element) {
      var formGroup = $(element).parent();
      formGroup.addClass('has-error');
      formGroup.find('.help-block').html(error);
    }, unhighlight: function(element, errorClass, validClass) {
      var formGroup = $(element).parent();
      formGroup.removeClass('has-error');
      formGroup.find('.help-block').html('');
    }
  });";
}