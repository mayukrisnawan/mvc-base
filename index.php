<?php

function appAutoload($name) {
  $paths = array(
    "controllers/".$name."Controller.php",
    "controllers/".$name.".php"
  );
  foreach ($paths as $path) {
    if (file_exists($path)) {
      require_once($path);
    }
  }
}
spl_autoload_register("appAutoload");
require_once "system/App.php";
App::run();