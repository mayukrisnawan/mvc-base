<?php

class Person extends Model {
  static $validates_presence_of = array(
    array("name", "message"=>"tidak boleh kosong")
  );
  /*
  static $validates_length_of = array(
    array("name", "maximum"=>10, "too_long"=>"terlalu panjang"),
    array("name", "minimum"=>5, "too_short"=>"terlalu pendek")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );*/
  static $validates_numericality_of = array(
    array("name", "only_integer"=>true, "message"=>"hanya boleh angka saja"),
    //array("name", "greater_than_or_equal_to"=>5, "message"=>"harus lebih dari atau sama dengan lima"),
    //array("name", "equal_to"=>5, "message"=>"harus sama dengan lima"),
    //array("name", "less_than_or_equal_to"=>10, "message"=>"harus kurang dari atau sama dengan sepuluh"),
    array("name", "less_than"=>10, "message"=>"harus kurang dari sepuluh"),
    //array("name", "equal_to"=>5, "message"=>"harus sama dengan lima"),
    array("name", "even"=>true, "message"=>"harus genap")
    //array("name", "odd"=>true, "message"=>"harus ganjil")
  );
}